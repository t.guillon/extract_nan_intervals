# extract_nan_intervals

## Description
The idea is to extract interval boundaries of missing values from a data table.

Say we have something like this:

| indices | values |
| ------ | ------ |
| 1      |   9     |
| 2.5    |   -1    |
| 3.1    |   nan   |
| 3.8    |   nan   |
| 4.1    |   8     |
| 5      |   nan   |
| 6      |   nan   |
| 6.8    |   nan   |
| 10     |   nan   |

We want to extract the intervals bound values `[3.1;3.8]` and `[5;10]`.

## Dependencies
This small script depends only on Python's [pandas](https://pandas.pydata.org/) and [numpy](https://numpy.org/), you will simply need to `pip install` both.

## Performances
The file comes with some synthetic data, including a ~ 5M values dataframe `./test_data/test_huge.gzip`. Results integrity can be tested with the `extract_nan_intervals.make_test()` function:
```python
In [1]: %timeit make_test()
1.37 s ± 6.35 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)
```
Notice that the runtime above also account for the data unzipping and assertion testing.
Performance of the sole intervals extraction can be obtained with:
```python
In [2]: %timeit extract_intervals(df_huge)
252 ms ± 1.59 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)
```


## License
Free of use, just give credit to myself @t.guillon.
