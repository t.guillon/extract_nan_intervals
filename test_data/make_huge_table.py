""" Making a large table with random nan intervals for testing perfs """
import numpy as np
import pandas as pd

def make_demo_df():
    headers = list('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
    ncols = len(headers)
    nrows = 200000
    # Index is strictly increasing and with a slight noise
    noise = np.random.uniform(-0.4, 0.4, size=nrows-1)
    index = np.arange(nrows, dtype=float)
    index[1:] += noise
    data = np.random.random((nrows, ncols))
    df = pd.DataFrame(data, index=index, columns=headers)
    # Adding nan intervals
    for col in df:
        nintervals = np.random.randint(10,100,1)[0]
        indices = np.random.choice(index, nintervals, replace=False)
        indices.sort()
        for istart,iend in zip(indices[::2], indices[1::2]):
            df[col].loc[istart:iend] = np.nan
    return df
    
if __name__ == '__main__':
    df = make_demo_df()
    df.to_csv('test_huge.gzip', compression='gzip')
    # np.savez_compressed('test_huge', data=df.values, index=index)