""" Extracting intervals of nan values from a data table """
import numpy as np
import pandas as pd

def extract_intervals(data):
    """ Extracting nan intervals in a dataframe
    
    Intervals will be extracted from each column, if multiple columns.
    Actually, only the dataframe indices bounding each interval are extracted. 
    
    Parameters
    ----------
    data: pd.DataFrame,
        The data to extract nan intervals from.
        Each column will be processed, it should contain only int/float values.
        
    Returns
    -------
    out: dict,
        The keys are the headers of the processed columns, and the values are
        the pairs of all nan interval bounds for each.
    
    """
    no_values = data.isnull()
    deltas = no_values.astype(int).diff()
    starts = deltas==1
    ends = (deltas==-1).shift(-1)
    starts.iloc[0] = no_values.iloc[0]
    ends.iloc[-1] = no_values.iloc[-1]
    bounds_vals = data.index
    intervals = {}
    for header in data.columns:
        dstarts = bounds_vals[starts[header]]
        dends = bounds_vals[ends[header]]
        dbounds = np.vstack((dstarts,dends)).T
        intervals[header] = dbounds
    return intervals
    
def make_test():
    """ Testing perfs and result """
    df = pd.read_csv(
        'test_data/test_huge.gzip', compression='gzip', index_col=0
        )
    intervals = extract_intervals(df)
    isnull = pd.DataFrame(
        np.zeros_like(df, dtype=bool), columns=df.columns, index=df.index
        )
    for header, interv_one_col in intervals.items():
        for istart, iend in interv_one_col:
            isnull[header].loc[istart:iend] = True
    assert all(df.isnull()==isnull)

if __name__ == '__main__':
    df = pd.read_excel('test_data/test.xlsx')
    data = df.set_index('Depth')
    intervals = extract_intervals(data)
    df_huge = pd.read_csv(
        'test_data/test_huge.gzip', compression='gzip', index_col=0
        )

